#include <stdio.h>
#include "vector.c"
#include <unistd.h>
int main() {
  // declare and initialize a new vector
  Vector vector;
  vector_init(&vector);

  double *test = malloc(sizeof(double) * 6);
  for (int i = 5; i >= 0; i--) {
    test[i] = (double) i * 2;
  }


  for (int i = 200; i > -50; i--) {
    vector_append(&vector, test);
  }

  // print out an arbitrary value in the vector
  printf("Heres the value at 22: %f\n", vector_get(&vector, 22)[4]);
  test[3] = 555.0;
  vector_set(&vector, 55555555, test);
  printf("Heres the value at 55555555: %f\n", vector_get(&vector, 55555555)[3]);
  printf("%d\n", sizeof(double) * vector.capacity * VECTOR_COLUMNS);

  // we're all done playing with our vector,
  // so free its underlying data array
  vector_free(&vector);

  printf("Freed: %d\n", 1);

  sleep(10);
  return 0;
}

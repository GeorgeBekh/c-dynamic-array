#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"

void vector_init(Vector *vector) {
  // initialize size and capacity
  vector->size = 0;
  vector->capacity = VECTOR_INITIAL_CAPACITY;

  // allocate memory for vector->data
  vector->data = malloc(sizeof(double) * vector->capacity * VECTOR_COLUMNS);
}

void vector_append(Vector *vector, double *value) {
  vector_double_capacity_if_full(vector);

  double *p = vector->data + vector->size++ * VECTOR_COLUMNS;

  memcpy(p, value, sizeof(double) * VECTOR_COLUMNS);
}

double *vector_get(Vector *vector, int index) {
  if (index >= vector->size || index < 0) {
    printf("Index %d out of bounds for vector of size %d\n", index, vector->size);
    exit(1);
  }

  return vector->data + index * VECTOR_COLUMNS;
}

void vector_set(Vector *vector, int index, double *value) {
  double *a = malloc(0);
  while (index >= vector->size) {
    vector_append(vector, a);
  }
  free(a);

  double *p = vector->data + index * VECTOR_COLUMNS;

  memcpy(p, value, sizeof(double) * VECTOR_COLUMNS);
}

void vector_double_capacity_if_full(Vector *vector) {
  if (vector->size >= vector->capacity) {
    // double vector->capacity and resize the allocated memory accordingly
    vector->capacity *= 2;
    vector->data = realloc(vector->data, sizeof(double) * vector->capacity * VECTOR_COLUMNS);
  }
}

void vector_free(Vector *vector) {
  free(vector->data);
}
